import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService, AuthState } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private readonly router: Router,
        private readonly authService: AuthService,
    ) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authService.state.getValue() === AuthState.AUTHENTICATED) { return true; }

        // Store the attempted URL for redirecting, maybe later ;-)
        // const url: string = state.url;
        // this.authService.redirectUrl = url;

        this.router.navigate(['/auth']);
        return false;
    }

}
