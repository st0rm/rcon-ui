import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService, AuthState } from '../auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'rcon-auth-form',
    styleUrls: ['form.component.scss'],
    template: `
        <form (ngSubmit)="onSubmit()" [formGroup]="form" class="content">

            <mat-form-field appearance="outline">
                <mat-label>Server Address</mat-label>
                <input matInput placeholder="Server Address" formControlName="url" />
            </mat-form-field>

            <mat-form-field appearance="outline">
                <mat-label>Username</mat-label>
                <input matInput placeholder="Username" formControlName="user" />
            </mat-form-field>

            <mat-form-field appearance="outline">
                <mat-label>Password</mat-label>
                <input matInput type="password" placeholder="Password" formControlName="password" />
            </mat-form-field>

            <button mat-flat-button color="primary" [disabled]="!form.valid" type="submit">Submit</button>

            <mat-progress-bar mode="indeterminate" *ngIf="authState === 1"></mat-progress-bar>

        </form>
  `,
})
export class AuthFormComponent {

    form = new FormGroup({
        url: new FormControl('', Validators.required),
        user: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
    });

    authState = null;

    constructor(
        private router: Router,
        private authService: AuthService
    ) {
        this.authService.state.subscribe(state => {
            this.authState = state;
            if (state === AuthState.DISCONECTED) {
                this.form.enable();
            } else {
                this.form.disable();
            }

            if (state === AuthState.AUTHENTICATED) {
                this.router.navigate(['connection']);
            }
        });
    }

    onSubmit() {
        const { url, user, password } = this.form.value;
        this.authService.login(url, user, password);
    }

}
