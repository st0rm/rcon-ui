import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import { WebsocketService } from '../shared/websocket.service';

export enum AuthState {
    DISCONECTED,
    PENDING,
    AUTHENTICATED,
}

export interface Message {
    action: string;
    data?: any;
}

@Injectable()
export class AuthService {

    public connection: WebSocket;
    public state = new BehaviorSubject<AuthState>(AuthState.DISCONECTED);
    public token = null;

    constructor(
        private readonly snackBar: MatSnackBar,
        private readonly wsService: WebsocketService
    ) {
        this.wsService.message.subscribe(message => {
            if (message.action === 'authenticated') {
                this.token = message.data;
                this.state.next(AuthState.AUTHENTICATED);
                this.snackBar.open('You are logged in');
            }
        });
        this.wsService.state.subscribe(state => {
            switch (state) {
                case WebSocket.CLOSING:
                case WebSocket.CLOSED:
                    this.state.next(AuthState.DISCONECTED);
                    this.snackBar.open('You are logged out');
                    break;

                case WebSocket.CONNECTING:
                    this.state.next(AuthState.PENDING);
                    break;
            }
        });
    }

    login(url, user, password) {

        this.wsService.connect(url)
            .then(() => {
                this.state.next(AuthState.PENDING);
                this.wsService.send({ action: 'login', data: { user, password } });
            }).catch(() => {
                this.state.next(AuthState.DISCONECTED);
            });

    }

    logout() {
        this.wsService.send({
            action: 'logout'
        });
        this.wsService.disconnect();
    }

}
