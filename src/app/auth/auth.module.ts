import { NgModule } from '@angular/core';

import { SharedModule } from './../shared/shared.module';
import { AuthService } from './auth.service';
import { AuthComponent } from './auth.component';
import { AuthFormComponent } from './form/form.component';

@NgModule({
    imports: [
        SharedModule,
    ],
    providers: [
        AuthService
    ],
    declarations: [
        AuthComponent,
        AuthFormComponent,
    ],
})
export class AuthModule {
}
