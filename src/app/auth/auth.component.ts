import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
    selector: 'rcon-auth',
    styleUrls: ['auth.component.scss'],
    template: `
        <mat-card>
        <mat-card-content>
            <rcon-auth-form></rcon-auth-form>
        </mat-card-content>
        </mat-card>
  `,
})
export class AuthComponent {

    constructor(public readonly authService: AuthService) { }

}
