import { Component } from '@angular/core';

import { ConnectionService } from '../connection.service';

@Component({
    selector: 'rcon-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})
export class InfoComponent {

    constructor(public readonly connectionService: ConnectionService) { }

}
