import { NgModule } from '@angular/core';

import { InfoComponent } from './info.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        InfoComponent,
    ],
    imports: [
        SharedModule,
    ]
})
export class InfoModule { }
