import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ConnectionRoutingModule } from './connection-routing.module';
import { InfoModule } from './info/info.module';
import { ChatModule } from './chat/chat.module';
import { TerminalModule } from './terminal/terminal.module';
import { ConnectionService } from './connection.service';
import { ConnectionComponent } from './connection.component';

@NgModule({
    imports: [
        SharedModule,
        ConnectionRoutingModule,
        InfoModule,
        ChatModule,
        TerminalModule,
    ],
    providers: [
        ConnectionService
    ],
    declarations: [
        ConnectionComponent,
    ],
})
export class ConnectionModule {
}
