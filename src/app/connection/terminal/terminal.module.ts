import { NgModule } from '@angular/core';

import { TerminalComponent } from './terminal.component';
import { SharedModule } from '../../shared/shared.module';
import { TerminalService } from './terminal.service';

@NgModule({
    declarations: [
        TerminalComponent,
    ],
    imports: [
        SharedModule,
    ],
    providers: [
        TerminalService
    ]
})
export class TerminalModule { }
