import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { WebsocketService, Message } from '../../shared/websocket.service';

@Injectable({
    providedIn: 'root'
})
export class TerminalService {

    output = new Subject<string>();
    // messages = [];

    constructor(private wsService: WebsocketService) {
        this.wsService.message.subscribe((message) => this.handleMessage(message));
    }

    handleMessage(message: Message) {
        switch (message.action) {
            case 'terminal.output':
                // this.messages.push(message.data);
                this.output.next(message.data);
                return;
        }

    }
}
