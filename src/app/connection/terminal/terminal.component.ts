import { Component, ViewChild, ElementRef, NgZone, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Terminal } from 'xterm';
import { fit } from 'xterm/lib/addons/fit/fit';

import { ConnectionService } from '../connection.service';
import { TerminalService } from './terminal.service';

const TERMINAL_CONFIG = {
    fontSize: 14,
    enableBold: true,
    cursorBlink: true,
    // theme: {
    //     cursor: 'rgb(0, 0, 0)'
    // }
};

@Component({
    selector: 'rcon-terminal',
    templateUrl: './terminal.component.html',
    encapsulation: ViewEncapsulation.ShadowDom,
    styleUrls: [
        './terminal.component.scss',
        '../../../../node_modules/xterm/dist/xterm.css'
    ]
})
export class TerminalComponent implements AfterViewInit {

    @ViewChild('container', { read: ElementRef })
    private readonly container: ElementRef;

    private xterm: Terminal;

    constructor(
        private readonly connectionService: ConnectionService,
        private readonly terminalService: TerminalService,
        private readonly elementRef: ElementRef,
        private readonly ngZone: NgZone
    ) {
        this.xterm = new Terminal(TERMINAL_CONFIG);

        terminalService.output.subscribe(output => {
            this.xterm.writeln(output);
        });
    }

    ngAfterViewInit() {
        this.xterm.open(this.container.nativeElement);
        fit(this.xterm);
    }

}
