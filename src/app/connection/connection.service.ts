import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { WebsocketService, Message } from '../shared/websocket.service';

@Injectable({
    providedIn: 'root'
})
export class ConnectionService {

    info = new BehaviorSubject<any>({ title: '' });

    constructor(private readonly wsService: WebsocketService) {
        this.wsService.message.subscribe((message) => this.handleMessage(message));

        this.wsService.send({ action: 'request', data: 'info' });
    }

    handleMessage(message: Message) {
        switch (message.action) {
            case 'info':
                this.info.next(message.data);
                return;
        }

    }
}
