import { NgModule } from '@angular/core';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { ChatComponent } from './chat.component';
import { SharedModule } from '../../shared/shared.module';
import { ChatService } from './chat.service';

@NgModule({
    declarations: [
        ChatComponent,
    ],
    imports: [
        SharedModule,
        ScrollingModule,
    ],
    providers: [
        ChatService
    ]
})
export class ChatModule { }
