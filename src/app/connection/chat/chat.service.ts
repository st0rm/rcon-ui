import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { WebsocketService, Message } from '../../shared/websocket.service';

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    message = new Subject<any>();

    constructor(private readonly wsService: WebsocketService) {
        this.wsService.message.subscribe((message) => this.handleMessage(message));
    }

    handleMessage(message: Message) {
        switch (message.action) {
            case 'chat.add':
                // this.messages.push(message.data);
                this.message.next(message.data);
                return;
        }

    }
}
