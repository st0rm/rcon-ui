import { Component, OnInit, ViewChild } from '@angular/core';

import { ChatService } from './chat.service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
    selector: 'rcon-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

    @ViewChild(CdkVirtualScrollViewport)
    viewport: CdkVirtualScrollViewport;

    batch = 20;
    theEnd = false;

    offset = new BehaviorSubject(null);
    infinite: Observable<any[]>;

    messages = [];

    constructor(private readonly chatService: ChatService) {
        // this.messages = this.chatService.messages;
        this.chatService.message.subscribe(message => {
            this.messages.push(message);
        });
    }

    ngOnInit() {
    }

    getBatch(offset) {
        console.log(offset);
        // return this.db
        //   .collection('people', ref =>
        //     ref
        //       .orderBy('name')
        //       .startAfter(offset)
        //       .limit(this.batch)
        //   )
        //   .snapshotChanges()
        //   .pipe(
        //     tap(arr => (arr.length ? null : (this.theEnd = true))),
        //     map(arr => {
        //       return arr.reduce((acc, cur) => {
        //         const id = cur.payload.doc.id;
        //         const data = cur.payload.doc.data();
        //         return { ...acc, [id]: data };
        //       }, {});
        //     })
        //   );
    }

    nextBatch(e, offset) {
        if (this.theEnd) {
            return;
        }

        const end = this.viewport.getRenderedRange().end;
        const total = this.viewport.getDataLength();
        console.log(`${end}, '>=', ${total}`);
        if (end === total) {
            this.offset.next(offset);
        }
    }

    trackByIdx(i) {
        return i;
    }

}
