import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from '../shared/not-found/not-found.component';
import { InfoComponent } from './info/info.component';
import { ChatComponent } from './chat/chat.component';
import { TerminalComponent } from './terminal/terminal.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'info',
        pathMatch: 'full',
    },
    {
        path: 'info',
        component: InfoComponent,
    },
    {
        path: 'terminal',
        component: TerminalComponent,
    },
    {
        path: 'chat',
        component: ChatComponent,
    },
    {
        path: '**',
        component: NotFoundComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ConnectionRoutingModule { }
