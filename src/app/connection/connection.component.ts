import { Component } from '@angular/core';

@Component({
    selector: 'rcon-connection',
    styleUrls: ['connection.component.scss'],
    template: `
        <router-outlet></router-outlet>
  `,
})
export class ConnectionComponent {

}
