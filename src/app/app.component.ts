import { Component } from '@angular/core';
import { AuthService, AuthState } from './auth/auth.service';

@Component({
    selector: 'rcon-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    isLoggedIn = false;

    constructor(public readonly authService: AuthService) {
        authService.state.subscribe(state => {
            this.isLoggedIn = (state === AuthState.AUTHENTICATED);
        })
    }

    logout() {
        this.authService.logout();
    }
}
