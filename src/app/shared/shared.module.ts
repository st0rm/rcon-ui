import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
    ],
    declarations: [
        NotFoundComponent
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,

        MatIconModule,
        MatCardModule,
        MatListModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatProgressBarModule,
    ]
})
export class SharedModule { }
