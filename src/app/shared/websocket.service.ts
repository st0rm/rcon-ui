import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

export interface Message {
    action: string;
    data?: any;
}

@Injectable()
export class WebsocketService {

    public connection: WebSocket;

    public state = new BehaviorSubject<number>(WebSocket.CLOSED);
    public message = new Subject<Message>();

    connect(url) {

        return new Promise((resolve, reject) => {
            this.connection = new WebSocket('ws://' + url);

            this.state.next(this.connection.readyState);

            this.connection.onmessage = (event) => this.handleMessage(event);
            this.connection.onerror = (err) => {
                this.resetConnection();
                reject(err);
            };
            this.connection.onclose = (err) => {
                this.resetConnection();
                reject(err);
            };
            this.connection.onopen = (message) => {
                this.state.next(this.connection.readyState);
                resolve(message);
            };
        });

    }

    disconnect() {
        this.connection.close();
        this.resetConnection();
    }

    handleMessage(event: MessageEvent) {
        const message = JSON.parse(event.data) as Message;

        this.message.next(message);
    }

    send(data: Message) {
        if (this.connection.readyState === WebSocket.OPEN) {
            this.connection.send(JSON.stringify(data));
        }
    }

    resetConnection() {
        if (!this.connection) return;

        this.connection.onmessage = null;
        this.connection.onerror = null;
        this.connection.onclose = null;
        this.connection.onopen = null;

        this.state.next(this.connection.readyState);
    }

}
